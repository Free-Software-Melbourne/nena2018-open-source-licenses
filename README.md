# Presentation on Software Licencing for NENA 2018
- Avaliable online with [gh-pages](https://free-software-melbourne.gitlab.io/nena2018-open-source-licenses/)

### To Run The Presentation
```bash
    npm install
    npm start -- --port=1947
```
or
```
./serve.sh
```

### To Access Speaker Notes

Press the "s" key


### reveal.js features used
- [nested slides](https://github.com/hakimel/reveal.js#markup)
- [Markdown contents](https://github.com/hakimel/reveal.js#markdown)
- [speaker notes](https://github.com/hakimel/reveal.js#speaker-notes)


