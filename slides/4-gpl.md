
### GPL: GNU General Public Licence
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Protecting users rights to
    - Study
    - Run
    - Modify
    - Redistribute
- Preserving those rights in the future

Note:
- GPL focuses on the freedoms and rights of the user of software, not on the cost
- nearly 300 words in the GPL2 and over 5000 words in GPL3
- but it all boils down to four essential freedoms that Free Software protect
- study, run, modify and redistribute
- Open source only protects the rights to study the source
- Protecting users and the communities rights and Preserving those rights
- Distribution triggers the responsibility to preserve these rights going forwards

---

### Lesser GPL - LGPL
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Allows binary linking with proprietary works

<br />

### Affero GPL - AGPL
<hr />

- Redistribution is expanded to include networked access
- Requires the operator of a service to provide the source code

Note:
- Two GPL variants worth mentioning are the lesser GPL and the Affero GPL
- Lesser GPL – LGPL, binary linking in proprietary works... the best of both worlds?
- Affero GPL – AGPL – issue with using but not distributing ... distributing the program by network (i.e. hosting a site) triggers distribution... most useful for hosted projects
