## Open Source Licences
## … and why do they matter?
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

Note:
- Welcome last session before afternoon tea
- I'm Ben Minerds, organiser of FSM and SFD in Melbourne
- Free software advocate and civic hacker
- Nuts and Bolts of licensing, whirlwind tour of the mechanics
- why it might matter for you to licence your software
- or be selective about the licensing of the software you use or package into your own work
- some case studies
