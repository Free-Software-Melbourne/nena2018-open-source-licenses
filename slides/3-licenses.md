### Dual licensing:
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Copyright owner can (re)license as they wish
- Can result in dual Open Source and Proprietary licensing
- Can require Contributor License Agreements

Note:
- Owner of the copyright re-licensing
- Open Source and Proprietary
- demand developers agree to a contributor license agreement... which can be a barrier to entry
- Linux Security patches dual license for the most up to date patches
- Developer tools: Oracle being the most predominant user, for example MySQL Community Edition and MySQL Enterprise Edition

---

### License families
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- MIT / Expat / BSD
- Apache - F/RaND
- GPL 2/3
- Niche, Unique and Vanity (Company Specific)

<img src=slides/img/Floss-license-slide-image.png width="70%">

Note:
- Most licences can be characterised as Permissive or Restrictive
- permissive characterised by minimal restrictions and size
- restrictive licenses take a more user centric approach and impose restrictions on devs and distributors to protect users
- we can also consider 4 main 'families'
- MIT & BSD
- Apache - Fair and Reasonable and non-discriminatory licensing
- GPL - restrictive
- Niche & Quirky & novelty
  - WTFPL
  - pay what you want
  - buy me a beer
  - postcard license
  - careware / donationware
  - Company specific vanity – sometimes useful for assigning rights to re-license
  - Unique... people just make them up

---

### MIT / Expat License
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Based on the Open Source principles
- Minimal restrictions
- Only Copyright, Attribution and a copy of the License
- Inclusion in a proprietary project is ok

Note:
- MIT - Open Source, not Free software
- Short sweet and to the point at 168 words and BSD only just tops 100, and easy to read
- Very few restrictions other than keeping the authorship clear and copyright assigned
- MIT is the basis of many license variants
- Allows inclusion in proprietary works
- BSD addresses issues in promotional material
