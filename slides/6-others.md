### Thing to Consider
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Why are you publishing
- What are your goals
- What are your expectations

Note:
- Which license you pick is highly dependant on your project and goals – sharing, communicating, helping, developing, enabling
- Is it important for the community to own this project more than just one individual or company – GPL protecting users
- Is there a prospect or would it be beneficial to have include or promote commercial involvmet or adoption of th project - MIT - conversely would it be ethical
- Are we likely to want to commercialize this or have it form the basis of a standard used by the whole world… or south east coast – F/RAND and Apache licincing might be an option


---

### Other Issues
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Patent Protection
- F/RAND and Standardisation
- Trademarks
- Copyright Assignment and Contributor License Agreement
- License Compatibility
- Enforcement
- Trends

Note:
- There are plenty of legal concerns we haven't covered today
  - Patents
  - F/RAND - Standards
  - contributor license agreements
  - Compatibility
  - Enforcement
