
### Creative Commons
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Grant users the right to use, modify and build on the art and docs
- allow the creator to restrict particular rights such as:
  - Attribution
  - Modification
  - Commercial use
- Software licences are not appropriate for docs / art

Note:
- Grants use, modification and development such as translations or inclusion in other works
- Allows some standardised restrictions on modification, attribution and commercial use
  - Attribution
  - Modification
  - Commercial use
- The Software licences we've discussed so far are not appropriate for docs and art. Don't use software licences
- Example with the permissive MIT: providing a copy of the licence with every distribution of the work is impractical for many mediums
