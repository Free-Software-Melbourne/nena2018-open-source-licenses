### Intellectual Property
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Copyright
- Trademark
- Patent Law
- Designs
- Plant Breeding Rights
- ...

Note:
- Intellectual Property can be a confusing mess
- IP is a class of somewhat related laws with many different aspects
- It's also highly dependant on jurisdiction and the laws of each country
- in general includes: copyright, tm, patent, designs
- plant breeding rights in Aus
- Overlaps: FireFox enforces TradeMark, but have exceptions in licence

---

### Copyright
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Are assigned automatically to the author
- Grant the creator exclusive rights for use and distribution
- Are a legal right created by the law of a country
- Unlicensed works are all rights reserved by default


Note:
- legal right, defined and granted by each country
- usually only for a limited time and varies by jurisdiction
- in Australia case we have automatic assignment and the term is generally 70+life
- grants exclusive rights to use and distribution
- all other rights are reserved... and this is where licensing comes to the rescue

---

### Licensing
<hr />
<header style="position: fixed;bottom: 15px; z-index:500; font-size:22px;background-color: rgba(0,0,0,0.5)">New Economy Network Australia 2018 - Strengthening the New Economy for the Common Good</header>

- Grants rights using Copyright law
- Can grant or deny specific rights
- Can bestow responsibilities or restrictions

Note:
- Open Source Licences use copyright to define legal terms of use
- licenses primarily use copyright law, granting rights to users that would be otherwise denied by copyright
- can selectively grant or deny very specific rights and conditions on uses to its users
- licences spread rights and responsibilities to users... viral nature
- defined by the author simply by being written down...
- but this causes can contribute to licence proliferation and discourage adoption
- EG: JSON "for good and not evil"
